from django.db import models
from django.http import HttpResponse
from django.template import loader
from django.template.context import RequestContext
from core.catalogue.forms import CatalogueForm

import slumber


def index(request):
    api = slumber.API('http://localhost:8000/api/v1/')
    form = CatalogueForm()
    ## api'dan cekilecek veriler forma baglanacak
    template = loader.get_template('home/index.html')
    context = RequestContext(request, {
        'form': form
    })
    return HttpResponse(template.render(context))