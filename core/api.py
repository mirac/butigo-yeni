from tastypie import fields
from tastypie.authorization import Authorization
from tastypie.resources import ModelResource
from catalogue.models import Catalogue


class CatalogueResource(ModelResource):
    parent = fields.ForeignKey('self', 'parent', null=True, full=True)

    class Meta:
        queryset = Catalogue.objects.all()
        resource_name = 'catalogue'
        authorization = Authorization()