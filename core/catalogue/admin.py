from django.contrib import admin
from core.catalogue.models import Catalogue


class CatalogueAdmin(admin.ModelAdmin):
    list_display = ('title', )


admin.site.register(Catalogue, CatalogueAdmin)