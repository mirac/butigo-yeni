from django.db import models


class Catalogue(models.Model):
    title = models.CharField(max_length=150)
    address = models.CharField(max_length=150)
    parent = models.ForeignKey('self', null=True, blank=True)

    def __unicode__(self):
        return self.title

    class Meta:
        app_label = "catalogue"